set fenc=UTF-8

set nobackup

set noswapfile

set autoread

set showcmd

set number

"set cursorline

"set cursorcolumn

set virtualedit=onemore

set smartindent

set visualbell

set showmatch

set laststatus=7

set wildmode=list:longest

nnoremap j gj
nnoremap k gk

syntax enable

set list listchars=tab:\▸\-

set expandtab

set tabstop=2

set shiftwidth=2

" 検索系
" 検索文字列が小文字の場合は大文字小文字を区別なく検索する
set ignorecase
" 検索文字列に大文字が含まれている場合は区別して検索する
set smartcase
" 検索文字列入力時に順次対象文字列にヒットさせる
set incsearch
" 検索時に最後まで行ったら最初に戻る
set wrapscan
" 検索語をハイライト表示
set hlsearch
" ESC連打でハイライト解除
nmap <Esc><Esc> :nohlsearch<CR><Esc>
