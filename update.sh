#!/bin/bash

# Vimの設定ファイルをコピーする

USER=$1

sudo cp -p .vimrc /Users/${USER}
sudo cp -p .gvimrc /Users/${USER}
